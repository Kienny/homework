import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchivedDashboardComponent } from './archived-dashboard.component';

describe('ArchivedDashboardComponent', () => {
    let component: ArchivedDashboardComponent;
    let fixture: ComponentFixture<ArchivedDashboardComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ArchivedDashboardComponent]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ArchivedDashboardComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
