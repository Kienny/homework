import { Component, OnInit } from "@angular/core";
import { TodoService } from "../../todo.service";
import { Todo } from "../../models/todo.interface";

@Component({
  selector: "app-archived-dashboard",
  templateUrl: "./archived-dashboard.component.html",
  styleUrls: ["./archived-dashboard.component.css"]
})
export class ArchivedDashboardComponent implements OnInit {
  archivedTodos: Todo[];

  constructor(private todoService: TodoService) {}

  ngOnInit() {
    this.todoService.getTodosByStatus(true).subscribe((data: Todo[]) => {
      this.archivedTodos = data;
    });
  }
}
