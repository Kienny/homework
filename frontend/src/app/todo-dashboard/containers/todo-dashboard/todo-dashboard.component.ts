import { Component, OnInit } from "@angular/core";
import { TodoService } from "../../todo.service";
import { Todo } from "../../models/todo.interface";
import { MatDialog } from "@angular/material";
import { InputDialogComponent } from "../../components/input-dialog/input-dialog.component";
import { filter, map } from "rxjs/operators";

@Component({
  selector: "app-todo-dashboard",
  templateUrl: "./todo-dashboard.component.html",
  styleUrls: ["./todo-dashboard.component.css"]
})
export class TodoDashboardComponent implements OnInit {
  todos: Todo[];
  description: string;

  constructor(private todoService: TodoService, public dialog: MatDialog) {}

  ngOnInit() {
    this.todoService
      .getTodosByStatus(false)
      .subscribe((data: Todo[]) => (this.todos = data));
  }

  handleArchive(event: Todo) {
    this.archiveTodo(event);
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(InputDialogComponent, {
      width: "450px",
      data: { description: this.description }
    });

    dialogRef
      .afterClosed()
      .pipe(
        filter(data => data !== undefined),
        map(data => {
          let todo = <Todo>{};
          todo.description = data;
          return todo;
        })
      )
      .subscribe(todo => {
        this.todoService.createTodo(todo).subscribe(data => {
          this.todos.push(data);
        });
      });
  }

  private archiveTodo(todo: Todo): void {
    let updatedTodo: Todo = Object.assign({}, todo);
    updatedTodo.archived = true;
    this.todoService.editTodo(updatedTodo).subscribe(data => {
      this.todos = this.todos.filter(item => item !== todo);
    });
  }
}
