import { TodoDashboardModule } from './todo-dashboard.module';

describe('TodoDashboardModule', () => {
  let todoDashboardModule: TodoDashboardModule;

  beforeEach(() => {
    todoDashboardModule = new TodoDashboardModule();
  });

  it('should create an instance', () => {
    expect(todoDashboardModule).toBeTruthy();
  });
});
