import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TodoDashboardComponent } from './containers/todo-dashboard/todo-dashboard.component';
import { RouterModule, Routes } from '@angular/router';
import { ArchivedDashboardComponent } from './containers/archived-dashboard/archived-dashboard.component';
import { HttpClientModule } from '@angular/common/http';
import { CustomMaterialModule } from '../shared/custom-material/custom-material.module';
import { InputDialogComponent } from './components/input-dialog/input-dialog.component';
import { FormsModule } from '@angular/forms';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { ArchivedListComponent } from './components/archived-list/archived-list.component';

const routes: Routes = [
    {
        path: 'todolist',
        children: [{path: '', component: TodoDashboardComponent}]
    },
    {
        path: 'archived',
        children: [{path: '', component: ArchivedDashboardComponent}]
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        HttpClientModule,
        CustomMaterialModule,
        FormsModule
    ],
    declarations: [
        TodoDashboardComponent,
        ArchivedDashboardComponent,
        InputDialogComponent,
        TodoListComponent,
        ArchivedListComponent
    ],
    entryComponents: [InputDialogComponent]
})
export class TodoDashboardModule {
}
