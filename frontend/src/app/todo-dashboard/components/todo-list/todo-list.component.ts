import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Todo } from "../../models/todo.interface";

@Component({
  selector: "app-todo-list",
  templateUrl: "./todo-list.component.html",
  styleUrls: ["./todo-list.component.css"]
})
export class TodoListComponent {
  @Input()
  todos: Todo[];

  @Output()
  archive: EventEmitter<Todo> = new EventEmitter<Todo>();

  constructor() {}

  onArchiveTodo(todo: Todo) {
    this.archive.emit(todo);
  }
}
