import { Component, OnInit, Input } from "@angular/core";
import { Todo } from "../../models/todo.interface";

@Component({
  selector: "app-archived-list",
  templateUrl: "./archived-list.component.html",
  styleUrls: ["./archived-list.component.css"]
})
export class ArchivedListComponent {
  @Input()
  archivedTodos: Todo[];

  constructor() {}
}
