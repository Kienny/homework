export interface Todo {
    id: number;
    description: string;
    archived: boolean;
    createdDate: number;
}
