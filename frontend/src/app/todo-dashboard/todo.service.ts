import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Todo } from "./models/todo.interface";
import { catchError } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class TodoService {
  readonly TODO_API: string = "/todos";

  constructor(private http: HttpClient) {}

  getTodosByStatus(archived: boolean): Observable<Todo[]> {
    return this.http
      .get<any>(`${this.TODO_API}?archived=${archived}`)
      .pipe(catchError(this.handleErrorObservable));
  }

  createTodo(todo: Todo): Observable<any> {
    return this.http
      .post(this.TODO_API, todo)
      .pipe(catchError(this.handleErrorObservable));
  }

  editTodo(todo: Todo): Observable<any> {
    return this.http
      .put(`${this.TODO_API}/${todo.id}`, todo)
      .pipe(catchError(this.handleErrorObservable));
  }

  handleErrorObservable(error: Response | any) {
    console.error(error.message || error);
    return Observable.throw(error.message || error);
  }
}
