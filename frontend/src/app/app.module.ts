import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TodoDashboardModule } from './todo-dashboard/todo-dashboard.module';
import { HomeComponent } from './home/home.component';
import { CustomMaterialModule } from './shared/custom-material/custom-material.module';

const routes: Routes = [
    {path: '', component: HomeComponent, pathMatch: 'full'}
];

@NgModule({
    declarations: [AppComponent, HomeComponent],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        CommonModule,
        RouterModule.forRoot(routes),

        TodoDashboardModule,

        CustomMaterialModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
