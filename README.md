To start application you need to have installed Docker and Docker-compose.

1. build backend from the backend directory: **./gradlew build buildDocker** 
2. build fronend from the frontend directory: **npm run docker:build**
3. start all containers from the project's root directory: **docker-compose up** 
