package lt.kennyo.todo.services;

import lt.kennyo.todo.dao.TodoDAO;
import lt.kennyo.todo.model.Todo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TodoServiceImplTest {

	@Mock
    TodoDAO todoDAOmock;

	@InjectMocks
	TodoServiceImpl todoServiceImpl;

	@Test
	public void findTodoById() {
		// Arrange
		Todo todo = new Todo("Dodododo");
		Optional<Todo> optionalTodo = Optional.of(todo);
		when(todoDAOmock.findTodo(1)).thenReturn(optionalTodo);

		// Act
		Todo expected = todoServiceImpl.findTodoById(1);

		// Assert
		assertEquals(expected.getDescription(), "Dodododo");
	}

	@Test
	public void findTodoById_givenNonExistingId_shouldReturnNull() {
		// Arrange
		Optional<Todo> optionalTodo = Optional.empty();
		when(todoDAOmock.findTodo(9999)).thenReturn(optionalTodo);

		// Act
		Todo expected = todoServiceImpl.findTodoById(9999);

		// Assert
		assertNull(expected);
	}

	@Test
	public void getTodos() {
		// Arrange
		List<Todo> mock = Arrays.asList(new Todo("Clean code")
				, new Todo("Refactor"));
		when(todoDAOmock.getTodos()).thenReturn(mock);

		// Act
		List<Todo> todo = todoServiceImpl.getTodos();

		// Assert
		assertThat(todo, is(mock));
		verify(todoDAOmock, times(1)).getTodos();
	}

	// test if custom id dont call
	@Test
	public void createTodo() {
		// Arrange
		Todo todo1 = new Todo("Todo 1");
		Todo todo2 = new Todo("Todo 2");
		when(todoDAOmock.saveTodo(todo1)).thenReturn(todo1);
		when(todoDAOmock.saveTodo(todo2)).thenReturn(todo2);
		when(todoDAOmock.getTodos()).thenReturn(Arrays.asList(todo1, todo2));

		// Act
		todoServiceImpl.createTodo(todo1);
		todoServiceImpl.createTodo(todo2);

		// Assert
		List<Todo> todos = todoServiceImpl.getTodos();
		assertEquals("Todo 1", todos.get(0).getDescription());
		assertEquals("Todo 2", todos.get(1).getDescription());
		verify(todoDAOmock, times(1)).getTodos();
		verify(todoDAOmock, times(1)).saveTodo(todo1);
		verify(todoDAOmock, times(1)).saveTodo(todo2);
	}

	@Test
	public void updateTodo() {
		// Arrange
		Todo todo = new Todo("Dudududu");
		todo.setArchived(true);
		when(todoDAOmock.updateTodo(todo)).thenReturn(todo);

		// Act
		todoServiceImpl.updateTodo(todo);

		// Assert
		verify(todoDAOmock, timeout(1)).updateTodo(todo);
	}

	@Test
	public void getTodosWithStatus() {
		// Arrange
		Todo todo1 = new Todo("Todo 1");
		Todo todo2 = new Todo("Todo 1");
		Todo todo3 = new Todo("Todo 1");
		List<Todo> todos = Arrays.asList(todo1, todo2, todo3);
		when(todoDAOmock.getTodosByArchive(true)).thenReturn(todos);
		when(todoDAOmock.getTodosByArchive(false)).thenReturn(todos);

		// Act
		todoServiceImpl.getTodos(true);
		todoServiceImpl.getTodos(false);

		// Assert
		verify(todoDAOmock, times(1)).getTodosByArchive(true);
		verify(todoDAOmock, times(1)).getTodosByArchive(false);
	}
}
