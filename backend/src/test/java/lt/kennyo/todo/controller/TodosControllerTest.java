package lt.kennyo.todo.controller;

import lt.kennyo.todo.model.Todo;
import lt.kennyo.todo.services.TodosService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TodosController.class)
public class TodosControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private TodosService todosService;

	@Test
	public void getTodos() throws Exception {
		// Arrange
		List<Todo> mockedTodos = new ArrayList<>();
		mockedTodos.add(new Todo("Todo 1"));
		mockedTodos.add(new Todo("Todo 2"));
		when(this.todosService.getTodos()).thenReturn(mockedTodos);

		// Act
		MvcResult result = mvc.perform(get("/todos").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andReturn();

		// Assert
		String responseString = result.getResponse().getContentAsString();
		assertTrue(responseString.contains("Todo 1"));
		assertTrue(responseString.contains("Todo 2"));
	}

	@Test
	public void getTodos_archivedTrue_shouldCalledTodoService() throws Exception {
		// Act
		makeFakeRequestWithArchivedParam("true", status().isOk());

		// Assert
		verify(todosService, times(1)).getTodos(true);
	}

	@Test
	public void getTodos_archivedFalse_shouldCalledTodoService() throws Exception {
		// Act
		makeFakeRequestWithArchivedParam("false", status().isOk());

		// Assert
		verify(todosService, times(1)).getTodos(false);
	}

	@Test
	public void getTodos_archivedAny_badRequest() throws Exception {
		// Act
		makeFakeRequestWithArchivedParam("xxx", status().isBadRequest());

		// Assert
		verify(todosService, times(0)).getTodos();
		verify(todosService, times(0)).getTodos(false);
		verify(todosService, times(0)).getTodos(true);
	}

	private void makeFakeRequestWithArchivedParam(String val, ResultMatcher status) throws Exception {
		mvc.perform(get("/todos")
				.accept(MediaType.APPLICATION_JSON)
				.param("archived", val))
				.andExpect(status)
				.andReturn();
	}

	@Test
	public void createTodo() throws Exception {
		// Arrange
		Todo todo = new Todo("Todo 1");
		when(todosService.createTodo(any(Todo.class))).thenReturn(todo);

		// Act
		MvcResult result = mvc.perform(post("/todos")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"description\": \"Todo 1\"}")
				.characterEncoding("UTF-8"))
				.andExpect(status().isOk())
				.andReturn();

		// Assert
		String responseString = result.getResponse().getContentAsString();
		assertTrue(responseString.contains("Todo 1"));
		assertTrue(responseString.contains("\"id\":0"));
		assertTrue(responseString.contains("\"archived\":false"));
	}

	@Test
	public void createTodo_invalidProperty_badRequest() throws Exception {
		String content = "{\"thisShouldFail\": \"todo 1\"}";
		mvc.perform(post("/todos")
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void updateTodo() throws Exception {
		// Arrange
		String content = "{\"description\":\"Important todo\", \"archived\": true}";
		Todo todo = todoMock();
		when(todosService.findTodoById(1)).thenReturn(todo);

		Todo todoFromRquestBody = mockedTodoFromRequestBody();
		setCurrentTodoValuesWithTodoFromRequest(todo, todoFromRquestBody);
		when(todosService.updateTodo(any(Todo.class))).thenReturn(todo);

		// Act
		MvcResult response = mvc.perform(put("/todos/1")
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
				.andExpect(status().isOk())
				.andReturn();

		String responseString = response.getResponse().getContentAsString();
		verify(todosService, times(1)).updateTodo(todo);
		assertTrue(responseString.contains("Important todo"));
		assertTrue(responseString.contains("\"id\":1"));
		assertTrue(responseString.contains("\"archived\":true"));
	}

	private Todo todoMock() {
		Todo todo = new Todo("Important todo");
		todo.setId(1);
		return todo;
	}

	private Todo mockedTodoFromRequestBody() {
		Todo todoFromRquestBody = new Todo("Important todo");
		todoFromRquestBody.setArchived(true);
		return todoFromRquestBody;
	}

	private void setCurrentTodoValuesWithTodoFromRequest(Todo todo, Todo todoFromRquestBody) {
		todo.setArchived(todoFromRquestBody.isArchived());
		todo.setDescription(todoFromRquestBody.getDescription());
	}

	@Test
	public void updatedTodo_givenIdThatDoesntExist_notFound() throws Exception {
		String content = "{\"description\":\"Important todo\", \"archived\": true}";
		when(todosService.findTodoById(9999)).thenReturn(null);
		mvc.perform(put("/todos/9999")
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
				.andExpect(status().isNotFound());
	}
}
