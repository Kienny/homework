package lt.kennyo.todo.dao;

import lt.kennyo.todo.model.Todo;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

@ActiveProfiles("dev")
@RunWith(SpringRunner.class)
@SpringBootTest
public class TodoDAOImplTest {

	@Autowired
	private Jdbi jdbi;

	@Autowired
	private TodoDAO todoDAO;

	@Before
	public void setUp() {
		List<Todo> todos = todosMock();
		Handle handle = jdbi.open();
		setupTodoTable(handle);
		populateTodoTable(handle, todos);
	}

	private List<Todo> todosMock() {
		Todo falseTodo = new Todo("Todo 999");
		falseTodo.setArchived(true);
		return Arrays.asList(new Todo("Todo 1"),
				new Todo("Todo 2"),
				new Todo("Todo 3"), falseTodo);
	}

	private void setupTodoTable(Handle handle) {
		String dropCreateTableSql = "DROP TABLE IF EXISTS Todo; " +
				"CREATE TABLE Todo (\n" +
				"  id SERIAL PRIMARY KEY,\n" +
				"  description TEXT,\n" +
				"  createdDate BIGINT,\n" +
				"  archived BOOLEAN\n" +
				");";
		handle.execute(dropCreateTableSql);
	}

	private void populateTodoTable(Handle handle, List<Todo> todos) {
		for (Todo todo : todos) {
			handle.execute("INSERT INTO Todo(description, createdDate, archived) VALUES (?, ?, ?)", todo.getDescription(), todo.getCreatedDate(), todo.isArchived());
		}
	}

	@Test
	public void findTodo() {
		Optional<Todo> todo = todoDAO.findTodo(1);
		assertTrue(todo.isPresent());
		assertEquals("Todo 1", todo.get().getDescription());
	}

	@Test
	public void findTodo_nonExistingId_empty() {
		Optional<Todo> todo = todoDAO.findTodo(9999);
		assertFalse(todo.isPresent());
		assertEquals(Optional.empty(), todo);
	}

	@Test
	public void getTodos() {
		List<Todo> todos = todoDAO.getTodos();
		assertEquals(4, todos.size());
		assertEquals("Todo 3", todos.get(2).getDescription());
	}

	@Test
	public void saveTodo() {
		List<Todo> todos = todoDAO.getTodos();
		assertEquals(4, todos.size());
		Todo savedTodo = todoDAO.saveTodo(new Todo("Rewatch GoT"));
		List<Todo> updatedTodos = todoDAO.getTodos();
		assertEquals(5, updatedTodos.size());
		assertEquals("Rewatch GoT", savedTodo.getDescription());
	}

	@Test
	public void updateTodo() {
		Todo todo = todoDAO.getTodos().get(0);
		assertFalse(todo.isArchived());
		todo.setArchived(true);
		Todo updatedTodo = todoDAO.updateTodo(todo);
		assertTrue(updatedTodo.isArchived());
	}

	@Test
	public void getTodosByArchive() {
		assertEquals(4, todoDAO.getTodos().size());

		List<Todo> todosByArchive = todoDAO.getTodosByArchive(true);
		assertEquals(1, todosByArchive.size());

		List<Todo> todosByFalseArchive = todoDAO.getTodosByArchive(false);
		assertEquals(3, todosByFalseArchive.size());
	}
}
