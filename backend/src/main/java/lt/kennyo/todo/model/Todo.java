package lt.kennyo.todo.model;

import org.jdbi.v3.core.mapper.reflect.ColumnName;
import org.jdbi.v3.core.mapper.reflect.JdbiConstructor;

import java.time.Instant;

public class Todo {

	private long id;
	private String description;
	private long createdDate;
	private boolean archived;

	public Todo() {
		this.createdDate = Instant.now().getEpochSecond();
		this.archived = false;
	}

	public Todo(String description) {
		this.description = description;
		this.createdDate = Instant.now().getEpochSecond();
		this.archived = false;
	}

	@JdbiConstructor
	public Todo(@ColumnName("id") long id,
				@ColumnName("description") String description,
				@ColumnName("createdDate") long createdDate,
				@ColumnName("archived") boolean archived) {
		this.id = id;
		this.description = description;
		this.createdDate = createdDate;
		this.archived = archived;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}

	public boolean isArchived() {
		return archived;
	}

	public void setArchived(boolean archived) {
		this.archived = archived;
	}

	@Override
	public String toString() {
		return "Todo{" +
				"id=" + id +
				", description='" + description + '\'' +
				", createdDate=" + createdDate +
				", archived=" + archived +
				'}';
	}
}
