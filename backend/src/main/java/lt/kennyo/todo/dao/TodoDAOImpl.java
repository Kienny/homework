package lt.kennyo.todo.dao;

import lt.kennyo.todo.model.Todo;
import org.jdbi.v3.core.Jdbi;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class TodoDAOImpl implements TodoDAO {

	private Jdbi jdbi;

	public TodoDAOImpl(Jdbi jdbi) {
		this.jdbi = jdbi;
	}

	@Override
	public Optional<Todo> findTodo(long id) {
		String sql = "SELECT * FROM Todo WHERE id = :id";
		return jdbi.withHandle(handle -> handle
				.createQuery(sql)
				.bind("id", id)
				.mapTo(Todo.class)
				.findFirst()
		);
	}

	@Override
	public List<Todo> getTodos() {
		String sql = "SELECT * FROM Todo";
		return jdbi.withHandle(handle -> handle
				.createQuery(sql)
				.mapTo(Todo.class)
				.list()
		);
	}

	@Override
	public Todo saveTodo(Todo todo) {
		String sql = "INSERT INTO Todo(description, createdDate, archived) VALUES(:description, :createdDate, :archived)";
		return jdbi.withHandle(handle -> handle.createUpdate(sql)
				.bind("description", todo.getDescription())
				.bind("createdDate", todo.getCreatedDate())
				.bind("archived", todo.isArchived())
				.executeAndReturnGeneratedKeys()
				.mapTo(Todo.class)
				.findOnly()
		);
	}

	@Override
	public Todo updateTodo(Todo todo) {
		String sql = "UPDATE Todo SET archived = :archived, description = :description WHERE id = :id";
		return jdbi.withHandle(handle -> handle.createUpdate(sql)
				.bind("archived", todo.isArchived())
				.bind("description", todo.getDescription())
				.bind("id", todo.getId())
				.executeAndReturnGeneratedKeys()
				.mapTo(Todo.class)
				.findOnly()
		);
	}

	@Override
	public List<Todo> getTodosByArchive(boolean archived) {
		String sql = "SELECT * FROM Todo WHERE archived = :archived";
		return jdbi.withHandle(handle -> handle
				.createQuery(sql)
				.bind("archived", archived)
				.mapTo(Todo.class)
				.list()
		);
	}
}
