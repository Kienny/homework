package lt.kennyo.todo.dao;

import lt.kennyo.todo.model.Todo;

import java.util.List;
import java.util.Optional;

public interface TodoDAO {

	Optional<Todo> findTodo(long id);

	List<Todo> getTodos();

	Todo saveTodo(Todo todo);

	Todo updateTodo(Todo todo);

	List<Todo> getTodosByArchive(boolean archive);
}
