package lt.kennyo.todo.controller;

import lt.kennyo.todo.model.Todo;
import lt.kennyo.todo.services.TodosService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TodosController {

	private TodosService todosService;

	public TodosController(TodosService todosService) {
		this.todosService = todosService;
	}

	@GetMapping("/todos")
	public ResponseEntity<List<Todo>> getTodos(@RequestParam(value = "archived", required = false) String archived) {
		if (archived != null) {
			if ("true".equals(archived) || "false".equals(archived)) {
				return new ResponseEntity<>(todosService.getTodos(Boolean.valueOf(archived)), HttpStatus.OK);
			} else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
			}
		}
		return new ResponseEntity<>(todosService.getTodos(), HttpStatus.OK);
	}

	@PostMapping("/todos")
	public ResponseEntity<Todo> createTodo(@RequestBody Todo todo) {
		return new ResponseEntity<>(todosService.createTodo(todo), HttpStatus.OK);
	}

	@PutMapping("/todos/{id}")
	public ResponseEntity<Todo> updateTodo(@PathVariable("id") long id,
										   @RequestBody Todo todo) {
		Todo currentTodo = todosService.findTodoById(id);
		if (currentTodo == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		currentTodo.setArchived(todo.isArchived());
		currentTodo.setDescription(todo.getDescription());
		return new ResponseEntity<>(todosService.updateTodo(currentTodo), HttpStatus.OK);
	}
}
