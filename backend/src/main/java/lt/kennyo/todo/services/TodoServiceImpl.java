package lt.kennyo.todo.services;

import lt.kennyo.todo.dao.TodoDAO;
import lt.kennyo.todo.model.Todo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TodoServiceImpl implements TodosService {

	private TodoDAO todoDAO;

	public TodoServiceImpl(TodoDAO todoDAO) {
		this.todoDAO = todoDAO;
	}

	@Override
	public Todo findTodoById(long id) {
		Optional<Todo> todoOptional = todoDAO.findTodo(id);
		return todoOptional.orElse(null);
	}

	@Override
	public List<Todo> getTodos() {
		return todoDAO.getTodos();
	}

	@Override
	public Todo createTodo(Todo todo) {
		return todoDAO.saveTodo(todo);
	}

	@Override
	public Todo updateTodo(Todo todo) {
		return todoDAO.updateTodo(todo);
	}

	@Override
	public List<Todo> getTodos(boolean archive) {
		return todoDAO.getTodosByArchive(archive);
	}
}
