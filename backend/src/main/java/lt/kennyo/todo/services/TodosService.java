package lt.kennyo.todo.services;

import lt.kennyo.todo.model.Todo;

import java.util.List;

public interface TodosService {

	Todo findTodoById(long id);

	List<Todo> getTodos();

	Todo createTodo(Todo todo);

	Todo updateTodo(Todo todo);

	List<Todo> getTodos(boolean archive);
}
