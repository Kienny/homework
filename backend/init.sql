CREATE TABLE Todo (
  id SERIAL PRIMARY KEY,
  description TEXT,
  createdDate BIGINT,
  archived BOOLEAN
);
